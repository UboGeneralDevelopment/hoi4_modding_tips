﻿
###on_action解説###

on_actionは単純で、ある行動(action)が行われたときに自動でコマンドなどが行われる。もちろん、条件指定はできる。
イベントの発生などでも使われ、内部的には
random_events={}
effects ={}
を発生させることができる。それぞれ文字通り特定のイベントもしくはエフェクトを発生させられる。エフェクトについてはもちろん条件分岐可能。
on_dailyなどは継続的にコマンドを実行するため、modiferの代わりとしても使える可能性がある。

effects = {
	ROOT = {
		if = {
			limit = { triggers }
			effects
		}
	}
}
てな感じだとtriggerを満たす国のみ効果が発動する。
ROOTはもちろんいろんなスコープに変更可能。国家タグ指定でもなんでも。

actionリストと主な使われ方は以下の通り

on_new_term_election # country 
on_government_change 
on_coup_succeeded

on_ace_promoted # Ace pilots# country, FROM = ace
on_aces_killed_each_other # country, FROM = ace, PREV = enemy ace # This event fires twice, once for each ace.
on_ace_killed_by_ace # country, FROM = our ace, # PREV = enemy ace, has killed FROM
on_ace_killed_other_ace # country, FROM = our ace, # PREV = enemy ace, killed by FROM
on_ace_killed # country, FROM = ace


on_justifying_wargoal_pulse # During justifying wargoals# triggered daily, so make sure there is a sink somewhere not firing events# country, FROM = target nation
on_wargoal_expire


on_nuke_drop


on_border_war_lost


on_leave_faction
on_create_faction #FROM is the one that joins the faction
on_offer_join_faction #FROM is country getting invited.
on_join_faction #FROM is faction leader on join faction requests. THIS DOES NOT FIRE ON ADD_TO_FACTION EFFECT! USE ON_OFFER_JOIN_FACTION!
on_faction_formed # When a new faction is formed


on_declare_war #FROM is war target
on_captulation  # ROOT is capitulated country, FROM is winner
on_uncapitulation # ROOT is previously capitulated country
on_peaceconference_ended #ROOT is winner #FROM is loser
on_annex #ROOT is winner #FROM gets annexed - For civil wars on_civil_war_end is also fired
on_civil_war_end #ROOT is winner #FROM gets annexed - This will also fire on_annex


on_puppet #used when puppeting in a peace conference#ROOT = nation being puppeted, FROM = overlord
on_release_as_puppet #used when puppeting through the occupied territories menu during peace time (or when releasing from non-core but owned territory, f.e. Britain releasing Egypt)#ROOT = nation being released, FROM = overlord
on_subject_free #ROOT is subject FROM is previous overlord
on_subject_autonomy_level_change #ROOT is subject FROM is overlord
on_subject_annexed #ROOT is subject FROM is overlord

on_state_control_changed #ROOT is new controller #FROM is old controller #FROM.FROM is state ID

on_unit_leader_level_up
on_army_leader_daily
on_army_leader_won_combat
on_army_leader_lost_combat
on_army_leader_promoted

on_bi_yearly_pulse # country random events
on_five_year_pulse 


よく使うのはこのあたり
on_startup
on_daily



あとは外交でのonactionが使えるかもしれないが不明。
military_access
offer_military_access
improve_relation
guarantee
non_aggression_pact
call_allies
join_allies
declare_war
create_faction
join_faction
offer_join_faction
peace_proposal
add_wargoal
lend_lease
incoming_lend_lease
send_volunteers
send_expeditionary_force
return_expeditionary_forces
request_expeditionary_forces
generate_wargoal
boost_party_popularity
stage_coup
leave_faction
dismantle_faction
ask_for_state_control
give_state_control
reduce_autonomy
increase_autonomy


