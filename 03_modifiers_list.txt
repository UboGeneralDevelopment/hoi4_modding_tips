﻿
static_modifier
relation_modifier
dynamic_modifier
state_modifierについてはここに記載

#########modifier
modifierはeffectやtriggerとはだいぶ異なるものとなっている。
modifierはそれが実装されるシステムに相当依存しており、多様。
modiferの情報はcommandやtriggerのようにまとまっておらずあちこち散逸しており、調べるのが大変。
そのため、ここではmodifierの種類ごとにその実装方法を解説する。


###static_modifiers###
wikiのmodifierに書いてあるmodifier。modifierが使えるmodシステムならば基本どこでも使えるが、その分制約が多い。
主に使えるところを表記すると以下の通り。

idea：modifiersにて記述する。基本的に国家レベルのmodifierを与える。
decision：days_removeの間効果を与えるmodifiersを与える。基本は国家レベル。
technology：普通にmodifiersを加えられる。特別な書式はなく、羅列する。
autonomous_states：modifiers内に記載する。通常のmodifierのほか、宗主国専用のtargeted_modifierも記載できる。t

###targeted_modifier###
やや特殊なモディファー。使えるシステムが限られている。

idea:modifier内にてtargeted_modifier宣言を行う。
targeted_modifier = {} において記述するが、tag指定が静的でダイナミックにはできない。

decisions:
targeted_modifierがtargeted_decisionsにおいて使えるらしい。要追記

autonomous_states
targeted_modifierはoverloadに限定されている。autonomous_states限定で使えるmodifierがある。


#effects

relation_modifier
modifiersフォルダ内にmodifierを記載したのち、add_relation_modifierで実行すること。
add_relation_modifierにおいてtargetを指定するが、ここはROOTなどを用いたダイナミック指定が可能。
でもevery_countryなどはできない。

targeted_modifiersの一覧。
attack_bonus_against
defense_bonus_against
cic_to_target_factor
mic_to_target_factor
trade_cost_for_target_factor
extra_trade_to_target_factor
license_purchase_cost
license_production_speed
license_tech_difference_speed
ai_license_acceptance
他にも使えるかもしれないので適宜追記

targeted_decisionにおいても対象をFROMとしてtargeted_modifierを使用可能である。



###state_modifiers###
add_state_modifierコマンド、occupation_policyなどによって実装する。ちなみにoccupation_policyはたぶん自国のcoreには適用されない。
state categoryによるmodifierの設定が可能。

確実に使える効果
local_resources
local_manpower
local_non_core_manpower
local_factories：dockyardとfactoryの数を増減する。増減は最終的な工場数にのみ反映され、スロットには現れない。
local_building_slots_factor
使えるかもしれない効果
resistance_tick
local_supplies
local_intel_to_enemies
local_factory_sabotage
attrition
local_building_slots
つかえたらいいなあ
local_monthly_population

non_coreにはlocal_building_slots_factor=-0.5とlocal_non_core_manpower=-0.98がかかっている。



#dynamic_modifiers
一日ごとにアップデート
dynamic_modifiersフォルダ内で宣言したのち、エフェクトで実装する。
書式は以下の通り、

モディファー宣言
dynamic_modifier_example = {
    political_power_factor = pp_factor_variable　#使いたいmodifierを書いて数値をvariableにする。名前も付けられる。
}
エフェクト
add_dynamic_modifier = { 
    modifier = dynamic_modifier_name #定義されたdynamic modifier
    scope = GER #目標となるスコープ。動的指定可能。国、リーダー、ステートどれでも可能
    days = 7 #期間。オプション
} 
remove_dynamic_modifier = {
    modifier = dynamic_modifier_name
    scope = GER #このスコープのdynamic_modifierのみを除去する。
}
force_update_dynamic_modifier = yes　#現在のスコープ内のdynamic_modifierを即時アップデート

トリガー
has_dynamic_modifier = {
    modifier = dynamic_modifier_name
    scope = GER #ターゲットが指定されていれば。
}
dynamic_modifierにおけるscopeはS参照する変数の保存されているスコープのことであり、targeted_modifierのtargetではない。




軍事系特殊modifiers
軍事系のmodifierは関連項目がおおく、ややこしい。こちらにはシステムと概要を思いつき次第列挙。

ideasやdicisionのstatic_modifier
通常のstatic_modifiersと同じ。modifiers内に記載。wikiのmodifiersに詳細が記載。

ideasのequipment_bonus
equipmentのstatsやmodifiersをinternal_typeごとに変更できる。wikiのideasに記載。
使用可能なmodifierはおそらくunitで使われているequipment_modifierに準拠

unitのequipment_modifier
unitごとにequipmentのstatsにmodifierを与える。支援中隊、騎馬隊、機械化歩兵等で使われている。
特別な書式がなしに羅列するため、unitの基本statsとequipment_modifierが混ざりやすく、要注意。

abilitiesのunit_modifiers
通常のstatic_modifiersをunit_leaderスコープ上で実行可能。add_temporaly_buff_to_unitsエフェクトを用いることもある。
unitで使われているequipment_modifiersは多分使えないが、よくわからない。
おそらくはここ専用の特殊なmodifierもつかえる。

technologies
internal_type、あるいは記述したunit_categoryごとにstatやmodifiersを変更する。
unitのstats、unitが装備するequipmentのstats双方に対してmodifierを与える事が可能。
通常のstatic_modifiersも使える。


