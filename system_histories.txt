﻿
実際に国を導入するためのもの。

基本的にはhistoryフォルダ中のstatesおよびにcountryが必要で、場合によってはunitsのoobを編集する。

countryでは、effectによって国家ステータスの初期設定を行う。編集する内容をバニラでの順に書き出すと

capital
oob
set_research_slots
set_stability
set_war_support
set_convoys
set_technology
add_political_power
complete_national_focus
add_ideas
set_politics={
parties
ruling_party
last_election
election_frequency
elections_allowed
}
create_country_leader
create_field_marshal
create_corps_commander
create_navy_leader
ってなかんじ。


statesではstatesの初期設定を行う。
所有権については、
owner　本来の所有者
controller　現在の所有者
core　コアがある
claim　主張されている
がある。
初期設定では首都と地続きのステートにownerが設定されていればそこはコアつきになる。

また、

1936.1.1={

}

などの年代設定で囲まれている箇所は、その年代でスタートした場合に実行される。
年代設定で囲まれていないのは、どの年代でスタートしても一番最初に実行され、年代設定があれば、上書きされる。
また、年代設定は建物などは過去の設定を引き継ぐ。

